# CNC-manufacturing

[Info générale](General.md) sur les fraiseuse CNC

[Tuto Shaper](Shaper.md)

[Tuto CNC Terminator](Terminator.md) (CNC-Step High-Z S-1000/T)

[Tuto Bantam](Bantam.md)

Pour nous aider a améliorer nos tutorielle vous pouvez le faire directement via GitLab ou en nous envoyant un mail a fablab@ulb.be