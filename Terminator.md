# CNC-Step High-Z - **Terminator**

Machine version: ```CNC-Step High-Z S-1000/T ```

Official [Operating Instructions](https://www.cnc-step.com/wp-content/uploads/2017-03-01-Operation-Instructions-High-Z-Standard-Series_EN.pdf)

[Fusion Product Documentation](https://help.autodesk.com/view/fusion360/ENU/?guid=GUID-1C665B4D-7BF7-4FDF-98B0-AA7EE12B5AC2)
[Training/Learning resources for Autodesk Fusion](https://www.autodesk.com/support/technical/article/caas/sfdcarticles/sfdcarticles/Training-Learning-resources-for-Fusion-360.html)


## Fusion - Design module

Create your 3D object in the design module.

CAD Design tutorials can be found [here](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad).

![](/img/Terminator/Design_module.png){width=70%}

## Fusion - Machine Setup & Post Process Setup

List of [post libraries](https://cam.autodesk.com/hsmposts) on AutoDesk website. 


<details>
<summary>What are post libraries ? - Click to expand</summary>

Post Libraries are used to install the correct machines into Fusion so it can correctly simulate the milling process.

</details>


<details>
<summary>### Post Process with automatic tool change</summary>

When in the *Manufacture* module, select the *Post Library* button in the *Manage* panel.

![](/img/Terminator/Manage2.png){width=70%}

Search for *KinetiC-NC* from CNC-STEP Vendor. 
Copy and paste it into to *Local* folder (you can also export and import it).

![](/img/Terminator/KinetiC.png){width=40%}

</details>


### Post Process with manual tool change

Download the following CPS file.

![Post Process CPS File](/Files/Terminator/CNC-STEP_kinetiC-NC_V-44115.cps)

When in the *Manufacture* module, select the *Post Library* button in the *Manage* panel.

![](/img/Terminator/Manage2.png){width=70%}

Go into the *Local* folder and select the *Import* tool.
Go to where you downloaded the previous CPS file and import it.

![](/img/Terminator/Import_PostProcess.png){width=40%}

## Fusion - Tool Setup

List of [Tools](https://cam.autodesk.com/hsmtools) on AutoDesk website.

In the *Manufacture* module, select the *Tool Library* button in the *Manage* panel.

![](/img/Terminator/ToolLib.png){width=40%}

Add the necessary End Mills that you are going to use.

![](/img/Terminator/ToolLib2.png){width=40%}


To understand more about Endmills see our [General](/General.md) documentation.

## Fusion - Manufacturing

[Autodesk Fusion 360 Basics: Manufacturing](https://www.autodesk.com/products/fusion-360/blog/autodesk-fusion-360-basics-manufacturing-fundamentals/)


Manufacture > Milling

![](/img/Terminator/Setup2.png){width=70%}

1. Select the *Setup* button in the Setup panel
    * Define the 0,0 coordinates position
    * Define the stock wood (or other material) size
    * Define the exported Post Process Name

![](/img/Terminator/Setup-Setup2.png){width=60%}

***

2. Setup the different milling or drilling operations.





***

![](/img/Terminator/Action-PP.png){width=70%}

3. Select the *Post Process* button in the Action panel
    * In *machine ahd post* category click on the folder icon and select the correct Post Process.

![](/img/Terminator/Action-PP2.png){width=50%}

***

4. In Action dropdown menu, select *Simulate* and verify your work.

## KinetiC-NC - PC App

[Web page](https://www.cnc-step.com/cnc-software/kinetic-nc-cnc-control-software/) of the program.

[User Manual](https://www.cnc-step.com/wp-content/uploads/2018-11-10-User-Manual-KinetiC-NC.pdf)