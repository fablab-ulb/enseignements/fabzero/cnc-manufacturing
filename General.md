# CNC 

[Tutoriel Mekanika](https://www.mekanika.io/fr_BE/blog/apprentissage-1)

[CNC calculateur](https://zero-divide.net/fswizard)

[Chip load chart](https://www.mekanika.io/web/image/15805-e46468af/Chip%20load%20table.PNG?access_token=cf7c3f83-56b7-439e-84a0-946aa79ee822)


## End Mills

End Mills cut rotationally in a horizontal,  or lateral (side to side) direction whereas a drill bit only cuts straight down, vertically into the material.

Contrary to drill bits where drill bits cut round holes straight down into the material by rotating them in a rotary drill.

### Functions of an End Mill

**Material Removal**: End mills are designed to efficiently remove material from a workpiece. They achieve this by cutting into the material along their flutes, creating chips and leaving behind the desired shape or surface finish.

**Profile Machining**: End mills are used for contouring and profiling, allowing machinists to create complex shapes, features, and pockets in workpieces with high precision.

**Finishing Operations**: End mills are essential for achieving a smooth and precise surface finish on workpieces. They can be employed in finishing passes to remove any remaining imperfections or roughness.

**Slotting and Slot Milling**: End mills are commonly used to cut slots or grooves in workpieces. They excel at creating narrow channels with precise dimensions.

**Drilling**: Some end mills, such as center-cutting end mills, can be used for drilling operations, enabling machinists to create holes with accuracy.

**Thread Milling**: Specialized end mills are designed for thread milling applications, allowing for the creation of threads in a workpiece.

### Anatomy of an End Mill

![](/img/general/anatomy.png){width=50%}

**Cutter Diameter**: The diameter of the theoretical circle formed by the cutting edges as the tool rotates.

**Shank Diameter**: The width of the shank that is held in the toolholder.

**Overall Length (OAL)**: The total length of the tool between both axial ends.

**Length of Cut/Flute Length**: The functional cutting depth with the tool in the axial orientation.

**Length Below Shank**: Also called the reach, is the length from the necked portion to the end of the cutting end of the tool.

**Neck Diameter**: The diameter of the neck.

**Helix Angle**: The angle measured from the centerline of the tool and a straight line tangent along the cutting edge.

![](/img/general/flutes.png){width=50%}

**Flutes**: The spiraled cutting grooves in the tool. Fewer flutes allow for larger chips and a deeper cut depth, but are weaker and best suited for plastics and aluminum. More flutes increase the tool strength but reduce the cutting depth for a smoother cut better suited for harder materials.

**Variable Helix**: A variable helix endmill utilizes unequal flute spacing to reduce harmonics which can greatly extend tool life and improve part finish.

![](/img/general/pitch.png){width=40%}

**Variable Pitch**: Pitch is the degree of radial separation between the cutting edges at a given point along the length of cut, most visible on the end of the end mill. Using a 4-flute tool with an even pitch as an example, each flute would be separated by 90°. Similar to a variable helix, variable pitch tools have non-constant flute spacing, which helps to break up harmonics and reduce chatter. The spacing can be minor but still able to achieve the desired effect. Using a 4-flute tool with variable pitch as an example, the flutes could be spaced at 90.5 degrees, 88.2 degrees, 90.3 degrees, and 91 degrees (totaling 360°).

### End Mill Profiles 

The profile refers to the shape of the cutting end of the tool. It is typically one of three options: square, corner radius, and ball.

**Square Profile**

Square profile tooling features flutes with sharp corners that are squared off at a 90° angle.

**Corner Radius / Bull Nose**

This type of tooling breaks up a sharp corner with a radius form. This rounding helps distribute cutting forces more evenly across the corner, helping to prevent wear or chipping while prolonging functional tool life. A tool with larger radii can also be referred to as “bull nose.”

**Ball Profile**

This type of tooling features flutes with no flat bottom, rounded off at the end creating a “ball nose” at the tip of the tool.

**Fish Tail**

Fish tail points prevent any splintering or breakout and will plunge directly into your material producing a flat surface.
These Router End Mills are ideal for plunge routing and producing precise contours – making them ideal for sign making and metal forming.

**Engraving V-Bits**

V-bits produce a “V” shaped pass and are used for engraving, particularly for making signs.
They come in a range of angles and tip diameters. 

**Roughing End Mills**

Great for large surface area work, roughing end mills have numerous serrations (teeth) in the flutes to quickly remove large amounts of material, leaving a rough finish.

They are sometimes referred to as Corn Cob cutters, or Hog Mills - so-called after the pig who ‘grinds’ away, or consumes, anything in its path, meaning they have high material removal rates.

**Straight Router Bits**

These TCT Straight Routers have extremely sharp cutting edges for plunge cutting, routing grooves, cutting dados, rabbeting and mortising and will achieve a clean, smooth, high quality surface finish.

**Chamfer End Mills**

 their design includes a side-cutting tool whose sole purpose is to create a taper or bevel in addition to chamfering your workpiece. We mostly apply it when we want to deburr sharp edges and give your workpiece an aesthetic appeal.




### Sources

* https://be-cu.com/blog/ball-nose-vs-flat-end-mill/
* https://www.harveyperformance.com/in-the-loupe/end-mill-anatomy/
* https://www.redlinetools.com/resources/endmill-information
* https://onmytoolings.com/bull-nose-end-mill-vs-ball-nose/
* https://eternaltools.com/blogs/tutorials/end-mills-the-essential-beginners-guide
* https://www.tsinfa.com/end-milling/


